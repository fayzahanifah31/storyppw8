from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

def home(request):
    response = {}
    return render(request,"main/home.html",response)
    
def fungsi_data(request):
    url= "https://www.googleapis.com/books/v1/volumes?q=" + request.GET["g"]
    ret = requests.get(url)
    print(ret.content)
    data = json.loads(ret.content)
    return JsonResponse(data, safe = False)
